<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

 

//Public Routes
Route::post('/login', 'AuthenticateController@login');



//Protected Routes
Route::middleware('jwt.verify')->group(function(){
	Route::resource('/user','UserController');
	Route::post('/{action}','TransactionController@store'); 
	Route::post('/logout', 'AuthenticateController@logout');	 
	Route::get('/getTransactionHistory','TransactionController@getTransactionHistory');

});
