<?php

use Illuminate\Database\Seeder;

class TransactionModesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction_mode')->insert([
        	[
        		'id'=>1,
        		'trans_type'=>'Deposit',
        		'created_at'=>date('Y-m-d H:i:s'),
        		'updated_at'=>date('Y-m-d H:i:s')
        	],

        	[

        		'id'=>2,
        		'trans_type'=>'Withdraw',        		 
        		'created_at'=>date('Y-m-d H:i:s'),
        		'updated_at'=>date('Y-m-d H:i:s')
        	],
            [

                'id'=>3,
                'trans_type'=>'Transfer',                
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]       	 

        	]);    
    }
}
