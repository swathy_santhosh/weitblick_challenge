<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[
        		'first_name'=>'Bruce',
        		'last_name'=>'Williams',
        		'email'=>'bruce_williams@example.com',
        		'phone_no'=>'3478132913',
        		'status'=>1,  
        		'created_at'=>date('Y-m-d H:i:s'),
        		'updated_at'=>date('Y-m-d H:i:s')
        	],

        	[        		 
        		'first_name'=>'Taylor',
        		'last_name'=>'Bundy',
        		'email'=>'taylor_bundy@example.com',
        		'phone_no'=>'3828363506',
        		'status'=>1,  
        		'created_at'=>date('Y-m-d H:i:s'),
        		'updated_at'=>date('Y-m-d H:i:s')
        	]

        	]);
    }
}
