<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cards')->insert([
        	[
        		'card_no'=>4444333322221111,
                'user_id'=>1,
        		'uuid'=>"5c476a28052cd",
        		'pin'=>password_hash('1234',PASSWORD_ARGON2I),
        		'status'=>1,
        		'valid_from'=>Carbon::createFromFormat('Y-m-d','2019-02-22'),
        		'valid_to'=>Carbon::createFromFormat('Y-m-d','2025-03-22'),
        		'account_balance'=>5000,
        		'overdraft_limit'=>500,
        		'created_at'=>date('Y-m-d H:i:s'),
        		'updated_at'=>date('Y-m-d H:i:s')
        	],

        	[
        		'card_no'=>4917610000000000,
                'user_id'=>2,
        		'uuid'=>"5c4f898b2fea8",
        		'pin'=>password_hash('1234',PASSWORD_ARGON2I),
        		'status'=>1,
        		'valid_from'=>Carbon::createFromFormat('Y-m-d','2018-02-22'),
        		'valid_to'=>Carbon::createFromFormat('Y-m-d','2026-03-22'),
        		'account_balance'=>15000,
        		'overdraft_limit'=>500,
        		'created_at'=>date('Y-m-d H:i:s'),
        		'updated_at'=>date('Y-m-d H:i:s')
        	]

        	]);    }
}
