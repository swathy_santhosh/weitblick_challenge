<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');    
            $table->integer('card_id')->unsigned();
            $table->foreign('card_id')->references('id')->on('cards');             
            $table->integer('amount');
            $table->integer('current_balance');
            $table->dateTime('date_of_transaction');
            $table->integer('transaction_mode_id')->unsigned();
            $table->foreign('transaction_mode_id')->references('id')->on('transaction_mode');
            $table->string('reason')->nullable();
            $table->boolean('is_transfer')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
