# Weitblick Full Stack Developer Test

The final POC for the Weitblick Developer Test. This Repository contains the API that powers the application. The Angular Client instructions will be provided in the Angular Client Repository.

## Prequisites

The below software are required to run the API Service

- Docker ( Refer documentation according to your OS)
Once the docker is installed, open the docker terminal and start the docker engine.
Navigate to the project directory and run the steps as mentioned below.

## Architecture

This solution is built based on the requirement to build ATM web app and is developed using Laravel with PHP 7.2-fpm , MYSQL as database and nginx as Webserver.It is a docker based solution so it can be run on docker container and can be spawned multiple times in Parallel as it uses a stateless authentication mechanism to protect endpoints that require user authentication using a JWT based filter mechanism.

The User would have to call the login endpoint first to get a JWT Token which should then be passed as an authorization header with a Bearer string so that the authenticated endpoints are accessible. The token is set to 60 minutes (ttl). In production system, this will be set to 15 minutes for security. After 1 hour, the token will be expired.

* Dependency Injection
The application uses dependency injection to isolate the service layer that can be independent of the core business logic. In this case, the TransactionInterface is implemented as an independent service that has the method declaration and process the logic. The idea is that, the implementation code can be changed anytime with a new implementation as long as it can take the same parameters and provide the output. 


All the routes are protected except login route. The password is secure and is hashed using PHP Argon hashing algorithm and the sensitive details such as card number are secured in the api response. The application is covered with unit test and integration test code and api document generator has been installed to view the list of endpoints. 

## List of Technology stack Used
- Docker                : LightWeight container to run application easily.
- Laravel framework     :  
- tymon/jwt-auth        : Generating and Parsing JWT Token. Helps in generating, parsing and managing the JWT Token.
- phpunit/phpunit       : Unit Testing 
- mockery/mockery       : Test Mocking

## Setup Instruction

(Make sure to start the docker terminal on your system first, before proceeding)
1.Run the install.sh script in the root of the project within the docker terminal: 

$ bash install.sh 
or
$ ./install.sh

This is all that's required to start and run the application as the install script bootstraps and automatically completes the steps required to run the application.

## Application Start up Workflow

1. When the docker container, comes up, it initializes the NGINX Server and gets ready to serve requests to localhost (or the container address) by sending the requests to web.php. 
2. It also spawns a MYSQL RDBMS process for persisting data
3. In addition to these steps, the installation file would run composer install to install any new packages that is required as a dependency
4. It will also run the migrations that would create the tables required with the columns and relevant keys
5. The next step will be to seed an initial set of data that is readily availble to be used
6. Once these steps are completed, the application is ready to accept API requests and return API responses based on the request, headers and parameters received

## IP Address of the Docker Engine (!!!!Important for Connecting the Client)
Make note of docker machine by running this below command in the docker terminal window:
$ docker-machine ip
You can test the endpoint in Postman or any RestClient using the ip from docker-machine.
For example, http://192.168.99.100/api/"{{routes}}"
Make sure that you add necessary headers.

## Database Architecture
The application uses MYSQL as the database to persist data. I have added scripts to run db seeding for testing. The user and card tables are loaded with sample of 2 records after executing scripts in install.sh.

For your easy reference, please find the card numbers and uuid of the card. Refer respective seeding file under src/database/seeds/<"filename">
card1:{
	'card_no': 4444333322221111,
	'uuid':5c476a28052cd
}

card2{
	'card_no':4917610000000000,
	'uuid':5c4f898b2fea8
}
You can use the above card numbers for testing application.

User table
==========
The user detail of the user such as User name, phone number and email,status are stored in users table. The status of the table is identified using 'status' flag field. Those who are active has status flag as 1 and inactive has  0.

Card table
=========
The currently logged in card(alis user) can deposit ,withdraw or transfer. Each table has the flag column 'status' to mark the status of the record. If the card 1 has status of 0, then it is inactive and vice-versa. 

Login History table
===================
The user will be able to login successfully if the status field is 1. The card and user status will be updated to 0 when the user 's card has been locked after 3 unsuccessful login attempts. The login history table will be updated for each failure login attempts until it has reached the count 3. 

Transaction &  Transfers
=======================
The fund transfer transaction is recorded in transfer table. The transaction history has the list of all the transactions that has taken place. In addition, for transfer, the data gets stored in transfer table to refer the source and destination card number.

## Running the Tests
1. To run the tests please execute the following command
$ docker-compose exec app vendor/bin/phpunit --stderr

## Complete API Documentation
Open the index.html under \public\docs folder to view the list of endpoints.