<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Card;

/**
 * Class for transaction controller test.
 */
class TransactionControllerTest extends TestCase
{


	/**
	 * Setup function to initialize test data
	 */
	public function setUp(){
		parent::setUp();
		$this->user = new Card(array(     		 
			'uuid'=>'5c476a28052cd',
			'id'=>1,
			'user_id'=>1
			));
		$this->token = \JWTAuth::fromUser( $this->user );
	}

	/**
	 * Function to test store transaction for self withdraw
	 */
	public function testStoreTransactionForSelfWithdraw(){
		$response = $this->withHeaders([
			'Authorization' => 'Bearer'.$this->token
			])->json('POST','/api/withdraw',[
			"amount"=>100,
			"is_transfer"=>0
			]);


			$response	
			->assertStatus(200)
			->assertJson($response->getData('data'))
			->assertSeeText("transaction_mode_id")
			->assertSeeText("dest_email");		 

		}



	/**
	 * Function to test store transaction for self deposit
	 */
	public function testStoreTransactionForSelfDeposit(){
		$response = $this->withHeaders([
			'Authorization' => 'Bearer'.$this->token
			])->json('POST','/api/deposit',[
			"amount"=>1000,
			"is_transfer"=>0
			]);


			$response	
			->assertStatus(200)
			->assertJson($response->getData('data'))
			->assertHeader("content-type","application/json")
			->assertJsonCount(12,"transaction")
			->assertSeeText("transaction_mode_id")
			->assertSeeText("dest_card_no")
			->assertSeeText("source_card_no");				 

		}

	/**
	 * Function to test store transaction for transfer
	 */
	public function testStoreTransactionForTransfer(){
		$response = $this->withHeaders([
			'Authorization' => 'Bearer'.$this->token
			])->json('POST','/api/transfer',[
			"amount"=>1,
			"is_transfer"=>1,
			"dest_id"=>"4917610000000000",
			"reason"=>"test transfer"
			]);

			$response	
			->assertStatus(200)
			->assertJson($response->getData('data'))
			->assertHeader("content-type","application/json")
			->assertJsonCount(13,"transaction")
			->assertSeeText("transaction_mode_id")
			->assertSeeText("dest_email")
			->assertSee("Taylor Bundy")
			->assertSee("4444XXXXXXXX1111")
			->assertSee("taylor_bundy@example.com");

		}
		
}
