<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \Mockery as Mockery;
use App\Interfaces\TransactionInterface;
use App\Transaction;
use App\Http\Controllers\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Card;
use App\Http\Requests\TransactionRequest;
 

/**
 * Class for transaction controller test.
 */
class TransactionControllerTest extends TestCase
{
    private $transaction_interface_mock = null;
    private $cardObj = null;
    private $request = null;
    private $response=null;

    /**
     * Setup function to initialize test data
     */
    public function setUp(){
    	parent::setUp();
    	$this->cardObj = new Card(array(     		 
    		'card_no'=>4444333322221111,
    		'uuid'=>'5c476a28052cd',
    		'account_balance'=>12000,
    		'overdraft_limit'=>100
    	));
        
    	$this->transaction_interface_mock = 
            Mockery::mock('App\Interfaces\TransactionInterface');
    	$this->transaction = new TransactionController($this->transaction_interface_mock);
        $this->transaction_request = new TransactionRequest();

    }

    /**
     * Function to test store transaction for deposit
     */
    public function testStoreForDeposit(){
    	$request = Request::create('/api/deposit/5c476a28052cd','POST',[
    		'amount'=>100 
    	]);

    	$transaction = new Transaction(array(    		 
    		'card_id'=> 5,
    		'amount'=>100,
    		'current_balance'=>12100,
    		'date_of_transaction'=>'2019-01-23 21:15:06',
    		'transaction_mode_id'=>1
    	));

    	$this->transaction_interface_mock->shouldReceive('storeTransaction')->andReturn($transaction);
        $response = $this->transaction->store($this->transaction_request ,'deposit');
        $content = json_decode($response->getcontent(),true);              
        $this->assertEquals($transaction->amount,$content['transaction']['amount']);
        $this->assertNotEquals(2000,$content['transaction']['amount']);
        $this->assertEquals($transaction->current_balance,$content['transaction']['current_balance']);
        $this->assertEquals($transaction->transaction_mode_id,$content['transaction']['transaction_mode_id']);
        $this->assertNotEquals(10,$content['transaction']['card_id']);
        $this->assertNotEquals(5,$content['transaction']['transaction_mode_id']);
    }

    /**
     * Function to test store transaction for withdraw
     */
    public function testStoreForWithdraw(){
        $request = Request::create('/api/withdraw/5c476a28052cd','POST',[
            'amount'=>1000 
        ]);

        $transaction = new Transaction(array(            
            'card_id'=> 5,
            'amount'=>1000,
            'current_balance'=>11100,
            'date_of_transaction'=>'2019-01-23 21:15:06',
            'transaction_mode_id'=>2
        ));

        $this->transaction_interface_mock->shouldReceive('storeTransaction')->andReturn($transaction);
        $response = $this->transaction->store($this->transaction_request,'withdraw');
        $content = json_decode($response->getcontent(),true);             
        $this->assertEquals($transaction->amount,$content['transaction']['amount']);
        $this->assertNotEquals(2000,$content['transaction']['amount']);
        $this->assertEquals($transaction->current_balance,$content['transaction']['current_balance']);
        $this->assertEquals($transaction->transaction_mode_id,$content['transaction']['transaction_mode_id']);
        $this->assertNotEquals(10,$content['transaction']['card_id']);
        $this->assertNotEquals(5,$content['transaction']['transaction_mode_id']);
    }

    /**
     * Function to assert store transaction for checking overdraft limit
     */
    public function testStoreForCheckingOverDraftLimit(){
        $request = Request::create('/api/withdraw/5c476a28052cd','POST',[
            'amount'=>100000 
        ]);

        $transaction = 'Overdraft limit reached.Cannot process this transaction.';       

        $this->transaction_interface_mock->shouldReceive('storeTransaction')->andReturn($transaction);
        $response = $this->transaction->store($this->transaction_request,'withdraw');
        $content = json_decode($response->getcontent(),true);                    
        $this->assertEquals($transaction,$content['transaction']);
        $this->assertNotEquals("Invalid",$content['transaction']);
    }

    /**
     * Function to assert transaction throws overdraft limit
     */
    public function testCheckingOverDraftLimitThrowsException(){
        $request = Request::create('/api/withdraw/5c476a28052cd','POST',[
            'amount'=>100000 
        ]);

        $message = "Overdraft limit reached.Cannot process this transaction.";

        $this->transaction_interface_mock->shouldReceive('storeTransaction')->andThrow('App\Exceptions\OverDraftLimitException',$message);
        $response = $this->transaction->store($this->transaction_request,'withdraw');
        $content = json_decode($response->getcontent(),true);           
        $this->assertEquals($message,$content['transaction']);
        
    }

    /**
     * Function to store transaction for transfer
     */
    public function testStoreForTransfer(){
        $request = Request::create('/api/transfer/5c476a28052cd','POST',[
                'amount'=>1000,
                'is_transfer'=> 1,
                'reason'=>"Personal expenses",
                'dest_id'=>"5c476b28051ab"
            ]); 
           

        $trans = new Transaction(array(
                'card_id'=> 5,
                'amount'=>1000,
                'current_balance'=>11000,
                'date_of_transaction'=>'2019-01-23 21:15:06',
                'transaction_mode_id'=>2,
                'dest_id'=>1,
                'source_card_no'=>"4444XXXXXXXX1111" 
                 
            ));
       

        $this->transaction_interface_mock->shouldReceive('storeTransaction')->andReturn($trans);
        $response = $this->transaction->store($this->transaction_request,'transfer');
        $content = json_decode($response->getcontent(),true); 
        $this->assertEquals($trans->amount,$content['transaction']['amount']);
        $this->assertNotEquals(2000,$content['transaction']['amount']);
        $this->assertEquals($trans->current_balance,$content['transaction']['current_balance']);
        $this->assertEquals($trans->transaction_mode_id,$content['transaction']['transaction_mode_id']);
        $this->assertContains('X',$content['transaction']['source_card_no']);
        $this->assertContains('XXXXXXXX',$content['transaction']['source_card_no']);
        $this->assertEquals($this->cardObj->account_balance-$content['transaction']['amount'],$content['transaction']['current_balance']);
    }

     

}
