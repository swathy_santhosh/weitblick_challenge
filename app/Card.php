<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use App\User;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class for Card Model to define the relationship between Card and other model.
 */
class Card extends Authenticatable implements JWTSubject
{
	/**
     * Gets the jwt identifier.
     *
     * @return     <type>  The jwt identifier.
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Function to get the jwt custom claims.
     *
     * @return     array  The jwt custom claims.
     */
    public function getJWTCustomClaims()
    {
        return [
        'id' => $this->id,
        'uuid' => $this->uuid,            
        'user_id' => $this->user_id           
        ];
    }


    /**
     * Sets the pin attribute.
     *
     * @param      <type>  $pin    The pin
     */
    public function setPinAttribute($pin)
    {
        if ( !empty($pin) ) {
            $this->attributes['password'] = password_hash($pin,PASSWORD_ARGON2I);        	 
        }
    }    

    /**
     * Function to define the relationship between card and the user model
     *
     * @return     <type>  ( associated user object for this card object )
     */
    public function user(){
    	return $this->belongsTo('App\User');
    }

    /**
     * Function to define the relationship between card and the transaction model
     *
     * @return     <type>  ( associated transaction object for this card object )
     */
    public function transactions(){
    	return $this->hasMany('App\Transaction');
    }

    protected $fillable =["uuid","card_no","account_balance","overdraft_limit"];

   /**
    * Hide the following fields when returning card object reponse
    *
    * @var        array
    */
   protected $hidden = array( 'pin', 'created_at', 'updated_at','card_no','user');

    /**
     * Variable to hold the array
     *
     * @var        array
     */
    protected $appends = array('user_card_no','user_name','user_email','user_phone');

    

    /**
     * Function to mask the card no attribute for security.
     *
     * @param      <type>  $value  The value
     *
     * @return     <type>  The user card no attribute.
     */
    public function getUserCardNoAttribute($value) {        
        $user_card_no = $this->card_no;       
        return substr_replace($user_card_no, str_repeat("X", 8), 4, 8);

    }

    
    /**
     * Function to get the user name attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     <type>  The user name attribute.
     */
    public function getUserNameAttribute($value) {
        $user_name = null;
        if ($this->user_id) { 
            $user_name = $this->getUser()->first_name." ".$this->user->last_name;
        } 
        return $user_name;
    }

    
    /**
     * Function to get the user email attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     <type>  The user email attribute.
     */
    public function getUserEmailAttribute($value) {
        $user_email = null;
        if ($this->user_id) {   
            $user_email = $this->getUser()->email;
        } 
        return $user_email;
    }


    /**
     * Function to get the user phone attribute.
     *
     * @param      <type>  $value  The value
     *
     * @return     <type>  The user phone attribute.
     */
    public function getUserPhoneAttribute($value) {
        $user_phone = null;
        if ($this->user_id) {   
            $user_phone = $this->getUser()->phone_no;
        } 
        return $user_phone;
    }

    /**
     * Function to get the user.
     *
     * @return     <type>  The user.
     */
    private function getUser(){
        return User::find($this->user_id);   
    }
}
