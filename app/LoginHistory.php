<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class for login history model.
 */
class LoginHistory extends Model
{
    protected $table = "login_history";

    protected $fillable =["card_id","failure_attempts"];

    public $timestamps = false;
}
