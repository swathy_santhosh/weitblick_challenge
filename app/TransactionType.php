<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class for transaction type model.
 */
class TransactionType extends Model
{
	// hide the following fields
    protected $hidden = array( 'is_self', 'created_at', 'updated_at');

    // add the dest_card_no attribute to the array
    protected $appends = array('dest_card_no');

    /**
     * Function to define relationship between transactiontype and transaction
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function transaction(){
      return $this->belongsTo('App\Transaction');
    }

  /**
   * Function to define relationship between transfer and transaction
   *
   * @return     <type>  ( description_of_the_return_value )
   */
    public function transfer(){
          return $this->belongsTo('App\Transfer');
    }    

    // code for $this->dest_card_no attribute
    public function getDestCardNoAttribute($value) {
        $dest_card_no = null;
        if ($this->transfer) {
            $dest_card_no = $this->transfer->dest_card_no;
         }
        return $dest_card_no;
    }
}
