<?php

namespace App\Exceptions;

use Exception;

/**
 * Exception for signaling over draft limit errors.
 * 
 * @author Swathy Santhosh
 */
class OverDraftLimitException extends Exception
{
    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {
        \Log::debug('Overdraft limit reached');
    }
}
