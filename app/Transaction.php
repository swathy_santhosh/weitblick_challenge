<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Card;

class Transaction extends Model
{
	// hide the following fields
    protected $hidden = array( 'pin', 'created_at', 'updated_at','card');

    protected $fillable =['card_id','amount','current_balance','date_of_transaction','transaction_mode_id'];

	public function transaction_mode(){
		return $this->belongsTo('App\TransactionMode');
	}
   	
   	public function card(){
		return $this->belongsTo("App\Card");
	}


    public function getCard(){
         return Card::where("id",$this->dest_id)->first();
     }
	 
	// add the source_card_no attribute to the array
    protected $appends = array('source_card_no','dest_name','dest_email','dest_card_no');

    // code for $this->source_card_no attribute
    public function getSourceCardNoAttribute($value) {
        $source_card_no = null;         
        if ($this->card) {
            $source_card_no = $this->card->card_no;
        }
        return substr_replace($source_card_no, str_repeat("X", 8), 4, 8);
       
    }

     // code for $this->dest_name attribute
    public function getDestNameAttribute($value) {
        $dest_name = null;
        if ($this->getCard()) {            
            $dest_name = $this->getCard()->user->first_name." ".$this->getCard()->user->last_name;
        }else{
            $dest_name = null;
        }
        return $dest_name;
    }

    // code for $this->dest_email attribute
    public function getDestEmailAttribute($value) {
        $dest_email = null;
        if ($this->getCard()) {            
            $dest_email = $this->getCard()->user->email;
        }else{
            $dest_email = null;
        }
        return $dest_email;
    }

    // code for $this->dest_card_no attribute
    public function getDestCardNoAttribute($value) {
        $dest_card_no = null;
        if ($this->getCard()) {            
            $dest_card_no = substr_replace($this->getCard()->card_no, 
                str_repeat("X", 8), 4, 8);
        }else{
            $dest_card_no = null;
        }
        return $dest_card_no;
    }
     
}
