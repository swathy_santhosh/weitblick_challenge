<?php

namespace App\Interfaces;

interface TransactionInterface{
	
	/**
	 * Method decalaration to store a transaction.
	 *
	 * @param      <type>  $request  The request
	 * @param      <type>  $action   The action	  
	 */
	public function storeTransaction($request,$action);

	/**
	 * Retrieves a transaction history.
	 */
	public function retrieveTransactionHistory();
}