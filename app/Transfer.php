<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class for transfer model to define the relationship between transfer and transaction.
 */
class Transfer extends Model
{ 

    /**
     * Function to define relationship between relationship between transfer and transaction
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function source(){
    	return $this->belongsTo('App\Transaction');
    }

    /**
     * Function to define relationship between relationship between transfer and transaction
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function dest(){
    	return $this->belongsTo('App\Transaction');
    }

  
}
