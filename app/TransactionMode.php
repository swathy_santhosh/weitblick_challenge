<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class for transaction mode model to define the relationship between TransactionMode  and other model.
 */
class TransactionMode extends Model
{
	protected $table = "transaction_mode";
}
