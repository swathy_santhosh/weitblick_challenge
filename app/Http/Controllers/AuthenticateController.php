<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Card;
use Config;
use JWTAuth;
use JWTFactory;
use Tymon\JWTAuthExceptions\JWTException;
use App\User;
use App\Http\Requests\AuthRequest;
use App\LoginHistory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;

/**
 * Class to implement the authentication 
 * 
 * @author Swathy Santhosh
 */
class AuthenticateController extends Controller
{

	/**
      * Create a new AuthController instance.
      * 
      * @param      \Illuminate\Http\Request  $request  The request
      */
    public function __construct(Request $request)
    {         
        $this->request = $request;
        
    } 

    /**
     * Get a JWT token via given credentials.
     *
     * @param      \App\Http\Requests\AuthRequest  $request  
     *
     * @return     \Illuminate\Http\JsonResponse
     */
    public function login(AuthRequest $request){
    	$credentials =  $request->only('card_no','password');         
        try{ 

            $card = Card::where("card_no",$credentials['card_no'])->where("status",1)->firstOrFail();           
            if(password_verify($credentials["password"],$card->pin)){                
                $token = JWTAuth::fromUser($card);                
                return response()->json(["card"=>$card,"token"=>$token]); 
            }else{
                return $this->trackLoginAttempts($card);               
            }  
        }catch(ModelNotFoundException $e){              
            return response()->json(['error' => 'Invalid Card ID.Check your card number'], 404);
        }
        catch(JWTException $e){              
           return response()->json(['error' => 'Unauthorized'], 401);
       }
   }
   

    /**
     * Function to track number of failure login attempts
     *
     * @param      <type>  $card   The card
     *
     * @return     <type>  ( json object stating error message )
     */
    private function trackLoginAttempts($card){ 
        $login_history = LoginHistory::updateOrcreate(['card_id'=>$card->id]);
        if(!$login_history->wasRecentlyCreated && $login_history->failure_attempts < 3){
            $login_history->failure_attempts = $login_history->failure_attempts+1;
            $login_history->updated_at = Carbon::now();
            $login_history->save();
            $this->updateCardStatus($card,$login_history);  
        } 
        return response()->json(['error' => 'Invalid Credentials.Your card will be locked after 3 failed attempts.'], 401);   
    }

    /**
     * Function to update the status of the card and user 
     *
     * @param      <type>  $card           The card
     * @param      <type>  $login_history  login history object
     */
    private function updateCardStatus($card,$login_history){
        if($login_history->failure_attempts == 3){
            $card->status =0;
            $card->save();
            $user = $card->user;
            $user->status = 0;
            $user->save();             
        }
        
    }

     /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
     public function logout()
     {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }


    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }
    
}
