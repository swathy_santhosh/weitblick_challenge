<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\TransactionInterface;
use App\Transaction;
use App\Http\Requests\TransactionRequest;
use Illuminate\Http\Response;
use App\Exceptions\OverDraftLimitException;

/**
 *  Class to handle transaction methods.
 *  
 *  @author Swathy Santhosh
 */
class TransactionController extends Controller
{


    // Initializing constructor      
    // @param      \App\Interfaces\TransactionInterface  $transaction 
    //
    public function __construct(TransactionInterface $transaction)
    {              
        $this->transaction = $transaction;   
    } 


    /**
     * Function to store transaction
     *
     * @param      \App\Http\Requests\TransactionRequest  $request  The request
     * @param      <type>                                 $action   The action 
     *
     * @return     <type>                                 ( json object )
     */
    public function store(TransactionRequest $request,$action){    
    	try{
    		$transaction = $this->transaction->storeTransaction($request,$action);   
    		return response()->json(["transaction"=>$transaction]);
    	}catch(OverDraftLimitException $exception){    		 
    		return response()->json([                 
                'transaction'=>"Overdraft limit reached.Cannot process this transaction."],400);
    	} 
    }

    /**
     * Gets the transaction history.
     *
     * @return     <type>  The transaction array.
     */
    public function getTransactionHistory(){
        $transactions = $this->transaction->retrieveTransactionHistory();
        return response()->json(["transactions"=>$transactions]);
    }

}
