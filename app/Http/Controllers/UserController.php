<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use JWTAuth;

class UserController extends Controller
{
    // Initializing constructor
    public function __construct()
    {              
     
    } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return response()->json(["users"=>$users]);
    }
    
}
