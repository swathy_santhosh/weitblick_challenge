<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [            
        'amount'=>'required|integer',
        'is_transfer'=>'required|integer',
        'reason'=>'required_if:is_transfer,1|string',
        'dest_id'=>'required_if:is_transfer,1|digits:16'
        ];
    }
}
