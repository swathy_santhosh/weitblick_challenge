<?php

namespace App\Services;

use App\Transaction;
use App\Interfaces\TransactionInterface;
use Carbon\Carbon;
use App\TransactionMode;
use App\Card;
use App\Exceptions\OverDraftLimitException;
use App\Transfer;
use App\TransactionType; 
use JWTAuth;

class TransactionServiceImpl implements TransactionInterface{

	/**
	 * Implementation method for storing a transaction.
	 *
	 * @param      <type>  $request  The request
	 * @param      <type>  $action   The action
	 * @return     <type>  ( json object of transaction )
	 */
	public function storeTransaction($request,$action)
	{
		$uuid = JWTAuth::getPayload()->get('uuid');		 
		switch ($this->getMode($action)) {
			case 1:
			return $this->saveDeposit($this->getMode($action),$request,$uuid);
			case 2:
			return $this->saveWithdraw($this->getMode($action),$request,$uuid);	
			case 3:					 
			return $this->saveTransfer($this->getMode($action),$request,$uuid);	
			default:
			return null;
		}		
	}

	/**
	 * Function to save a deposit.
	 *
	 * @param      <type>  $mode     The mode
	 * @param      <type>  $request  The request
	 * @param      <type>  $uuid     The uuid
	 *
	 * @return     <type>  ( returns json object of deposit transaction )
	 */
	private function saveDeposit($mode,$request,$uuid) {
		$balance = $this->getCardDetail($uuid)->account_balance + $request->input('amount');
		return $this->saveTransaction($mode,$request,$uuid,$balance);	
	}

	/**
	 * Function to save a withdraw.
	 *
	 * @param      <type>                                   $mode     The mode
	 * @param      <type>                                   $request  The request
	 * @param      <type>                                   $uuid     The uuid
	 *
	 * @throws     \App\Exceptions\OverDraftLimitException  (Userdefined exception to throw when overdraft limit is reached)
	 *
	 * @return     <type>                                   ( return json object of withdraw transaction )
	 */
	private function saveWithdraw($mode,$request,$uuid){
		if($this->checkAmtWithinOverDraftLimit($request->input('amount'),$uuid)){
			$balance = $this->getCardDetail($uuid)->account_balance - $request->input('amount');
			return $this->saveTransaction($mode,$request,$uuid,$balance);	
		} else {
			throw new OverDraftLimitException('Overdraft limit reached.Cannot process this transaction');	
		}
	}

	/**
	 * Function to save a transfer.
	 *
	 * @param      <type>  $mode     The mode
	 * @param      <type>  $request  The request
	 * @param      <type>  $uuid     The uuid
	 *
	 * @return     <type>  ( returns json object of trannsfer transaction )
	 */
	private function saveTransfer($mode,$request,$uuid) {
		$dest_user = $this->checkIfCardExists($request->input("dest_id"));	
		$sourceTxn = null;	 	 
		if($dest_user){				  	
			DB::transaction(function() use ($mode,$request,$uuid,$dest_user,&$sourceTxn){
				$sourceTxn = $this->saveWithdraw(2,$request,$uuid);		
				$targetTxn = $this->saveDeposit(1,$request,$dest_user->uuid);	
				$this->makeTransfer($sourceTxn->id,$targetTxn->id);	
				$sourceTxn->dest_id = $targetTxn->card_id;
				 	
			});
			return $sourceTxn;		
		} 		
	}

	/**
	 * Function to saves a transaction object on transaction table.
	 *
	 * @param      <type>       $mode     The mode
	 * @param      <type>       $request  The request
	 * @param      <type>       $uuid     The uuid
	 * @param      <type>       $balance  The balance
	 *
	 * @return     Transaction  ( return transaction )
	 */
	private function saveTransaction($mode,$request,$uuid,$balance){
		$amount = $request->input('amount');
		$source = $this->getCardDetail($uuid);
		$transaction = new Transaction;
		$transaction->card_id =  $source->id;
		$transaction->date_of_transaction = Carbon::now();		
		$transaction->amount = $amount;	
		$transaction->current_balance = $balance;
		$transaction->transaction_mode_id = $mode;
		$transaction->reason = $request->input("reason");
		$transaction->is_transfer = $request->input("is_transfer");
		$transaction->save();		 		
		$this->setAccountBalance($transaction->current_balance,$uuid);
		return $transaction;
	}

	/**
	 * Function to get the card detail.
	 *
	 * @param      <type>  $uuid   The uuid
	 *
	 * @return     <type>  The card detail.
	 */
	private function getCardDetail($uuid){
		return Card::where("uuid",$uuid)->firstOrFail();
	}

	/**
	 * Function to update  the account balance.
	 *
	 * @param      <type>  $balance  The balance
	 * @param      <type>  $uuid     The uuid
	 */
	private function setAccountBalance($balance,$uuid){
		$card = Card::where("uuid",$uuid)->firstOrFail();
		$card->account_balance = $balance;
		$card->updated_at = Carbon::now();
		$card->save();
	}

	/**
	 * Function to get the mode of transaction.
	 *
	 * @param      <type>  $action  The action
	 *
	 * @return     <type>  The mode.
	 */
	private function getMode($action){
		return TransactionMode::where('trans_type', strtolower($action))
		->select("id")->firstOrFail()->id;

	}

	/**
	 * Function to check if the amount is within overdraft limit
	 *
	 * @param      integer  $amount  The amount
	 * @param      <type>   $uuid    The uuid
	 *
	 * @return     boolean  ( returns the boolean value of the condition)
	 */
	private function checkAmtWithinOverDraftLimit($amount,$uuid){
		$overdraft_amt = $this->getCardDetail($uuid)->overdraft_limit;	
		$account_balance = $this->getCardDetail($uuid)->account_balance;
		return $account_balance-$amount >= -($overdraft_amt);
	}

	/**
	 * Functio to make a transfer.
	 *
	 * @param      <type>  $src_id   The source identifier
	 * @param      <type>  $dest_id  The destination identifier
	 */
	private function makeTransfer($src_id,$dest_id){
		$transfer = new Transfer;
		$transfer->source_id = $src_id;
		$transfer->dest_id = $dest_id;
		$transfer->save();	
	} 

	/**
	 * Function to check if the card exists in database
	 *
	 * @param      <type>  $dest_id  The destination card number
	 *
	 * @return     <type>  ( Returns the card object )
	 */
	private function checkIfCardExists($card_id){
		$card = Card::where("card_no",$card_id)->firstOrFail();
		return $card;
	}

	/**
	 * Retrieves a transaction history.
	 *
	 * @return     <type>  ( transaction array )
	 */
	public function retrieveTransactionHistory(){
		$current_uuid = JWTAuth::getPayload()->get('uuid');
		$card = $this->getCardDetail($current_uuid);		
		return Transaction::where("card_id",$card->id)->orderBy('date_of_transaction','desc')->get();

	}
}