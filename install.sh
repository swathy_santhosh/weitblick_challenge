#!/bin/bash
echo "Installation Script for the Weitblick Challenge";
echo "Copying env file"
cp .env.example .env
echo "Initiating Composer Install...";
composer install
echo "Finished running Composer Install";
echo "Running docker-compose"
docker-compose up -d
echo "Finished running docker compose command... Process running in background";
echo "Initiating DB Migration..."
docker-compose exec app php artisan migrate
echo "Finished running DB Migration"
echo "Initiating DB Seeding..."
docker-compose exec app php artisan db:seed
exit
